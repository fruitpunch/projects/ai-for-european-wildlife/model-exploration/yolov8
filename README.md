# YOLOv8

DATE: 10th June 2023

## Getting started

The files in the repo are organized as follows: 

1. Data Preparation 

 ```Data Preparation _v2.ipynb``` : This file can be used for preparing the training folders. 

YOLOv8 and YOLO-NAS has a slighly different folder structure for the training pipeline. Please check the comments in the jupyter notebook. 

For YOLOv8: the image and their annotation are in the same folder 

For YOLO-NAS: the image and label folders are separate. 

Both use the YOLO format of annotation; ie. x(center), y(center), width and height


2. YOLOv8 Training and Inference 

There are two notebooks, one each for training and inference 
```
YOLOv8 Training.ipynb
YOLOv8 Inference.ipynb
```
3. YOLO-NAS Training and Inference

There are two notebooks, one each for training and inference

```
YOLO-NAS Training.ipynb
YOLO-NAS Inference 3.1.1.ipynb
```

***

